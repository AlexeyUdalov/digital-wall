'use strict';
let mockDataUrl = 'img/up-logo.png';
let chooseApply = document.querySelector('.js-apply-choose'),
    chooseCancel = document.querySelector('.js-cancel-choose'),
    buyPixelButton = document.querySelector('.js-buy-pixel'),
    wall = document.querySelector('.js-wall');

const app = {
  typePay: 'card',
  amount: 0,
  busyPixels: [],
  cost: 0,
  currentPixel: {},
  _step: 0,
  get step() {
    return this._step;
  },
  set step(value) {
    if ((typeof value).toLocaleLowerCase() === 'number') {
      this._step = value;
    } else {
      console.error('Передан не верный тип данных, должен быть Number');
    }
  },
  // Записываем занятые пикселы
  setBusyPixel(pixel) {
    this.busyPixels.push({
      top: parseInt(pixel.position.top, 10),
      left: parseInt(pixel.position.left, 10),
      bottom: parseInt(pixel.position.top, 10) + parseInt(pixel.height, 10),
      right: parseInt(pixel.position.left, 10) + parseInt(pixel.width, 10),
    });
  },
  // Начало покупки пикселов
  start(e) {
    e.preventDefault();
    let progress = document.querySelector('.js-progress'),
      buyPixelButton = document.querySelector('.js-buy-pixel'),
      wallContent = document.querySelector('.js-wall-content');

    app.step = 1;
    buyPixelButton.style.display = 'none';
    progress.classList.add('active');
    $(wallContent).mCustomScrollbar('stop');
    app.openStepPopup(app.step);
  },
  // Выход с первого шага
  exit(e) {
    e.preventDefault();
    let progress = document.querySelector('.js-progress'),
      buyPixelButton = document.querySelector('.js-buy-pixel'),
      wallContent = document.querySelector('.js-wall-content');

    progress.classList.remove('active');
    buyPixelButton.style.display = 'block';
    app.closeStepPopup(app.step);
    app.step = 0;
  },
  // Выбор пикселов
  startChoosePixels(e) {
    e.preventDefault();
    let progressStep1 = document.querySelector('.js-progress .js-progress-step-1'),
      wallInfo = document.querySelector('.js-wall-info');

      setTimeout(function() {
        $(wallInfo).fadeIn();
        setTimeout(function() {
          $(wallInfo).fadeOut();
        }, 3000);
      }, 500);

      progressStep1.classList.add('success');
      app.closeStepPopup(app.step);
  },
  // Открываем попап
  openStepPopup(step) {
    let wallContainer = document.querySelector('.js-wall-container');

    switch(step) {
      case 1: {
        let popup = document.querySelector('template').content.querySelector('.js-popup-step-1').cloneNode(true),
        nextButton = popup.querySelector('.js-next'),
        backButton = popup.querySelector('.js-back');

        backButton.addEventListener('click', app.exit);
        nextButton.addEventListener('click', app.startChoosePixels);
        wallContainer.append(popup);
        $(popup).fadeIn();
        break;
      }
      case 2: {
        let popup = document.querySelector('template').content.querySelector('.js-popup-step-2').cloneNode(true),
        nextButton = popup.querySelector('.js-next'),
        fileForm = popup.querySelector('.js-file-form'),
        backButton = popup.querySelector('.js-back'),
        uploadFile = popup.querySelector('.js-upload-file');

        nextButton.addEventListener('click', app.sendFileForm);
        backButton.addEventListener('click', app.prevStep);
        uploadFile.addEventListener('change', app.addFileName);
        fileForm.addEventListener('submit', function(e) {e.preventDefault();})
        wallContainer.append(popup);
        $(popup).fadeIn();
        break;
      }
      case 3: {
        let popup = document.querySelector('template').content.querySelector('.js-popup-step-3').cloneNode(true),
        nextButton = popup.querySelector('.js-next'),
        backButton = popup.querySelector('.js-back'),
        payTypes = Array.from(popup.querySelectorAll('.js-type-pay')),
        payAmount = popup.querySelector('.js-amount');

        for (let i=0; i < payTypes.length; i++) {
          let radio = payTypes[i].querySelector('input[type="radio"]');
          radio.checked = radio.value === app.typePay;
          payTypes[i].addEventListener('click', function() {
            app.typePay = this.querySelector('input[type="radio"]').value;
          });
        };

        payAmount.innerText = `${app.amount} р`;
        nextButton.addEventListener('click', app.nextStep);
        backButton.addEventListener('click', app.prevStep);
        wallContainer.append(popup);
        $(popup).fadeIn();
        break;
      }
      case 4: {
        let popup, payButton, backButton;

        if (app.typePay === 'account') {
          popup = document.querySelector('template').content.querySelector('.js-popup-step-4').cloneNode(true);
          payButton = popup.querySelector('.js-pay');
          backButton = popup.querySelector('.js-back');
        } else if (app.typePay === 'card') {
          popup = document.querySelector('template').content.querySelector('.js-popup-email').cloneNode(true);
          payButton = popup.querySelector('.js-pay');
          backButton = popup.querySelector('.js-back');
        }
        backButton.addEventListener('click', app.prevStep);
        payButton.addEventListener('click', app.goToPay);
        wallContainer.append(popup);
        $(popup).fadeIn();
        break;
      }
    }
  },
  // Закрываем попап
  closeStepPopup(step) {
    switch(step) {
      case 1: {
        let popup = document.querySelector('.js-wall-container .js-popup-step-1'),
          nextButton = popup.querySelector('.js-next'),
          backButton = popup.querySelector('.js-back');

        backButton.removeEventListener('click', app.exit);
        nextButton.removeEventListener('click', app.startChoosePixels);
        $(popup).fadeOut();
        setTimeout(function() {
          popup.remove();
        }, 500);
        break;
      }
      case 2: {
        let popup = document.querySelector('.js-wall-container .js-popup-step-2'),
          uploadFile = popup.querySelector('.js-upload-file'),
          nextButton = popup.querySelector('.js-next'),
          backButton = popup.querySelector('.js-back');

        nextButton.removeEventListener('click', app.sendFileForm);
        backButton.removeEventListener('click', app.prevStep);
        uploadFile.removeEventListener('change', app.addFileName);
        $(popup).fadeOut();
        setTimeout(function() {popup.remove()}, 500);
        break;
      }
      case 3: {
        let popup = document.querySelector('.js-wall-container .js-popup-step-3'),
          nextButton = popup.querySelector('.js-next'),
          backButton = popup.querySelector('.js-back');

        backButton.removeEventListener('click', app.prevStep);
        nextButton.removeEventListener('click', app.nextStep);
        $(popup).fadeOut();
        setTimeout(function() {popup.remove()}, 500);
        break;
      }
      case 4: {
        let popup, backButton, payButton;

          if (app.typePay === 'account') {
            popup = document.querySelector('.js-wall-container .js-popup-step-4');
            payButton = popup.querySelector('.js-pay');
            backButton = popup.querySelector('.js-back');
          } else if (app.typePay === 'card') {
            popup = document.querySelector('.js-wall-container .js-popup-email');
            payButton = popup.querySelector('.js-pay');
            backButton = popup.querySelector('.js-back');
          }

        backButton.removeEventListener('click', app.prevStep);
        payButton.removeEventListener('click', app.goToPay);
        $(popup).fadeOut();
        setTimeout(function() {popup.remove()}, 500);
        break;
      }
    }
  },
  // Переходим на следующий шаг
  nextStep() {
    switch(app.step) {
      case 1: {
        let progressStep2 = document.querySelector('.js-progress .js-progress-step-2'),
          selectedPixels = document.querySelector('.js-selected-pixel');

        app.step = 2;
        progressStep2.classList.add('active');
        app.openStepPopup(app.step);
        selectedPixels.style.top = app.currentPixel.position.top + 'px';
        selectedPixels.style.left = app.currentPixel.position.left + 'px';
        selectedPixels.classList.add('active');
        selectedPixels.style.width = app.currentPixel.width + 'px';
        selectedPixels.style.height = app.currentPixel.height + 'px';
        break;
      }
      case 2: {
        let progressStep2 = document.querySelector('.js-progress .js-progress-step-2'),
          progressStep3 = document.querySelector('.js-progress .js-progress-step-3'),
          selectedPixels = document.querySelector('.js-selected-pixel'),
          selectedActions = selectedPixels.querySelector('.js-selected-action');

        app.step = 3;
        selectedPixels.classList.add('saved');
        selectedActions.classList.remove('active');
        progressStep2.classList.add('success');
        progressStep3.classList.add('active');
        app.openStepPopup(app.step);
        break;
      }
      case 3: {
        app.closeStepPopup(app.step);
        app.step = 4;
        app.openStepPopup(app.step);
        break;
      }
    }
  },
  // Переход на предыдущий шаг
  prevStep() {
    switch(app.step) {
      case 2: {
        let progressStep2 = document.querySelector('.js-progress .js-progress-step-2'),
          selectedPixels = document.querySelector('.js-selected-pixel'),
          choosePixel = document.querySelector('.js-choose-pixel');

        progressStep2.classList.remove('active');
        selectedPixels.style.width = 0;
        selectedPixels.style.height = 0;
        setTimeout(function() {selectedPixels.classList.remove('active')}, 300);
        choosePixel.classList.add('active');
        app.closeStepPopup(app.step);
        app.step = 1;
        break;
      }
      case 3: {
        let progressStep3 = document.querySelector('.js-progress .js-progress-step-3'),
          progressStep2 = document.querySelector('.js-progress .js-progress-step-2'),
          selectedPixels = document.querySelector('.js-selected-pixel'),
          selectedActions = selectedPixels.querySelector('.js-selected-action');

        progressStep3.classList.remove('active');
        progressStep2.classList.remove('success');
        selectedPixels.classList.remove('saved');
        selectedActions.classList.add('active');
        app.closeStepPopup(app.step);
        app.step = 2;
        break;
      }
      case 4: {
        app.closeStepPopup(app.step);
        app.step = 3;
        app.openStepPopup(app.step);
        break;
      }
    }
  },
  // Выводим название загруженного файла
  addFileName() {
    let nameBlock = this.parentElement.previousElementSibling,
      curFile = this.files;

    if (curFile.length) {
      nameBlock.innerText = curFile[0].name;
    }
  },
  // Отправляем картинку на сервер
  sendFileForm() {
    let fileForm = document.querySelector('.js-file-form'),
      formData = new FormData(fileForm);

    if (formData.get('file').size) {
      axios({
        method: 'post',
        url: '/uploads',
        data: formData,
        headers: {'Content-Type': 'multipart/form-data' }
      }).then(function(response) {
        let selectedPixels = document.querySelector('.js-selected-pixel'),
          selectedImage = selectedPixels.querySelector('.js-selected-image'),
          selectedActions = selectedPixels.querySelector('.js-selected-action'),
          selectedSaveButton = selectedPixels.querySelector('.js-selected-save'),
          selectedChangeButton = selectedPixels.querySelector('.js-selected-change'),
          image = document.createElement('img');

        app.currentPixel.image = response.data.image;
        image.src = response.data.image;
        selectedImage.append(image);
        selectedActions.classList.add('active');
        selectedSaveButton.addEventListener('click', app.nextStep);
        selectedChangeButton.addEventListener('click', app.backToUploadImage);
        app.closeStepPopup(app.step);
      }).catch(function(error) {
        if (error.request.status === 404) {
          // Показываем ошибку
          let fileError = document.querySelector('.js-file-error');
          if (!fileError.classList.contains('active')) {
            fileError.classList.add('active');
          }
        }
      });
    }

    // Код для тестирования
    // let selectedPixels = document.querySelector('.js-selected-pixel'),
    //   selectedImage = selectedPixels.querySelector('.js-selected-image'),
    //   selectedActions = selectedPixels.querySelector('.js-selected-action'),
    //   selectedSaveButton = selectedPixels.querySelector('.js-selected-save'),
    //   selectedChangeButton = selectedPixels.querySelector('.js-selected-change'),
    //   image = document.createElement('img');

    // app.currentPixel.image = mockDataUrl;
    // image.src = mockDataUrl;
    // selectedImage.append(image);
    // selectedActions.classList.add('active');
    // selectedSaveButton.addEventListener('click', app.nextStep);
    // selectedChangeButton.addEventListener('click', app.backToUploadImage);
    // app.closeStepPopup(app.step);
  },
  // Возвращаемся на загрузку картинки
  backToUploadImage() {
    let selectedPixels = document.querySelector('.js-selected-pixel'),
      selectedActions = selectedPixels.querySelector('.js-selected-action'),
      image = selectedPixels.querySelector('img');

    selectedActions.classList.remove('active');
    image.remove();
    app.openStepPopup(app.step);
  },
  // Оплата
  goToPay() {
    // Перенаправляем на оплату в зависимости от app.typePay
    switch(app.typePay) {
      case 'account': {
        let requisiteForm = document.querySelector('.js-requisite-form'),
          formElements = Array.from(requisiteForm.elements),
          isRequired = validRequisite(formElements);

        if (isRequired) {
          for (let i = 0; i < formElements.length; i++) {
            app.currentPixel[formElements[i].name] = formElements[i].value;
          }

          axios.post('/reserve', app.currentPixel)
            .then(function(response) {
              let wallContainer = document.querySelector('.js-wall-container'),
                progressStep3 = document.querySelector('.js-progress .js-progress-step-3'),
                popup = document.querySelector('template').content.querySelector('.js-popup-pay-account').cloneNode(true),
                closePopup = popup.querySelector('.js-close-popup');
              app.closeStepPopup(app.step);
              progressStep3.classList.add('success');
              closePopup.addEventListener('click', function(e) {
                e.preventDefault();
                setTimeout(function() {popup.remove()}, 500);
              });
              wallContainer.append(popup);
              $(popup).fadeIn();
            })
            .catch(function(error) {
              if (error.request.status === 404) {
                // Показываем ошибку о занятом месте
                document.querySelector('.js-pay-info').style.display = 'none';
                document.querySelector('.js-reserve-error').style.display = 'block';
                document.querySelector('.js-pay').disabled = true;
              }
            });
        }
        break;
      }
      case 'card': {
        let emailForm = document.querySelector('.js-email-form'),
          formElements = Array.from(emailForm.elements),
          isRequired = validRequisite(formElements);

        if (isRequired) {
          for (let i = 0; i < formElements.length; i++) {
            app.currentPixel[formElements[i].name] = formElements[i].value;
          }
          axios.post('/reserve', app.currentPixel)
            .then(function(response) {
              // Если место свободно
              app.closeStepPopup(app.step);
              // Редирект на оплату
              window.location.href = response.data.urlpay;
            })
            .catch(function(error) {
              if (error.request.status === 404) {
                // Показываем ошибку о занятом месте
                document.querySelector('.js-pay-info').style.display = 'none';
                document.querySelector('.js-reserve-error').style.display = 'block';
                document.querySelector('.js-pay').disabled = true;
              }
            });
        }
        break;
      }
    }
  }
};

function validRequisite(elements) {
  let emailField = elements.filter(function(item) {return item.type === 'email';}),
    validEmail = emailField[0].value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i),
    reqFields = !elements.filter(function(item) {return item.required;}).some(function(item) {return item.value === '';});

    if (!validEmail) {
      emailField[0].classList.add('error');
    } else {
      emailField[0].classList.remove('error');
    }

  return reqFields && validEmail;
};

if (chooseCancel) {
  // Отменяем выбор пикселов
  chooseCancel.addEventListener('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    let choosePixel = document.querySelector('.js-choose-pixel'),
      chooseInfo = choosePixel.querySelector('.js-choose-info');
    app.currentPixel = {};
    app.amount = 0;
    choosePixel.classList.remove('error');
    $(choosePixel).animate({
      width: '50px',
      height: '50px',
    }, 300);
    chooseApply.disabled = false;
    chooseInfo.innerText = '';
  });
}

if (chooseApply) {
  // Сохраняем выбранные пикселы
  chooseApply.addEventListener('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    let choosePixel = document.querySelector('.js-choose-pixel');

    app.currentPixel.position = {
      top: choosePixel.offsetTop,
      left: choosePixel.offsetLeft,
    };
    app.amount = Math.round((choosePixel.offsetWidth / 50) * (choosePixel.offsetHeight / 50) * app.cost);
    app.currentPixel.width = choosePixel.offsetWidth;
    app.currentPixel.height = choosePixel.offsetHeight;
    choosePixel.classList.remove('active');
    app.nextStep();
  });
}

if (buyPixelButton) {
  buyPixelButton.addEventListener('click', app.start);
}

if (wall) {
  wall.addEventListener('click', function(e) {
    e.stopPropagation();
    let choosePixel = this.querySelector('.js-choose-pixel'),
      applyButton = choosePixel.querySelector('.js-apply-choose'),
      chooseInfo = choosePixel.querySelector('.js-choose-info');

    if (app.step === 1 && e.target.classList.contains('wall__pixel')) {
      if (choosePixel.classList.contains('active')) {
        choosePixel.style.top = e.target.offsetTop + 'px';
        choosePixel.style.left = e.target.offsetLeft + 'px';
        choosePixel.classList.remove('error');
        applyButton.disabled = false;
        $(choosePixel).animate({
          width: '50px',
          height: '50px',
        }, 300);
        chooseInfo.innerText = '';
      } else {
        choosePixel.style.top = e.target.offsetTop + 'px';
        choosePixel.style.left = e.target.offsetLeft + 'px';
        choosePixel.classList.add('active');
      }
    }
  });
}

export default app;
