'use strict';

let logoTemplate = document.querySelector('template').content.querySelector('.wall__logo');

let wallInterface = {
  renderLogo(logoData) {
    let logoElement = logoTemplate.cloneNode(true);

    if (logoData.status === 'booked') {
      logoElement.classList.add('booked');
    }

    if (logoData.url) {
      let isCorrect = logoData.url.match(/https:\/\/|http:\/\/?/g);

      if (!isCorrect) {
        logoElement.querySelector('a').href = 'http://' + logoData.url;
      } else {
        logoElement.querySelector('a').href = logoData.url;
      }
    }

    logoElement.querySelector('img').dataset.src = logoData.image;
    logoElement.style.top = logoData.position.top + 'px';
    logoElement.style.left = logoData.position.left + 'px';
    logoElement.style.width = logoData.width + 'px';
    logoElement.style.height = logoData.height + 'px';

    return logoElement;
  }
}

export default wallInterface;
