'use strict';

import wallInterface from './wall.js';
import app from './app.js';

$(document).ready(function() {
  const PIXEL_WIDTH = 50;
  const PIXEL_HEIGHT = 50;
  const Month = {
    0: `января`,
    1: `февраля`,
    2: `марта`,
    3: `апреля`,
    4: `мая`,
    5: `июня`,
    6: `июля`,
    7: `августа`,
    8: `сентября`,
    9: `октября`,
    10: `ноября`,
    11: `декабря`,
  };
  let chooseHandl = document.querySelector('.js-choose-handler'),
    openPopup = document.querySelector('.js-open-popup'),
    relevantOpen = document.querySelector('.js-open-relevant'),
    startX, startY, startWidth, startHeight;

  axios.get('data/data.json')
    .then(function(response) {
      updateCompanyCounter(response.data.company);
      app.cost = response.data.cost;
      return response;
    })
    .then(function(response) {
      let wall = document.querySelector('.js-wall');
      if (wall) {
        var cartFragment = document.createDocumentFragment();

        Array.from(response.data.pixels).forEach(function(pixel) {
          cartFragment.appendChild(wallInterface.renderLogo(pixel));
          app.setBusyPixel(pixel);
        });

        wall.prepend(cartFragment);
        lazyload();
        checkDevice();
      }
    });

  if ($('.js-popup-nav').length) {
    $('.js-popup-nav').on('click', function(e) {
      e.preventDefault();
      $(this).addClass('active').siblings().removeClass('active');
      $('.js-info-popup').find('.js-popup-content').eq($(this).index()).addClass('active').siblings().removeClass('active');
    });
  }
  if ($('.js-popup-content').length) {
    var popupContents = document.querySelectorAll('.js-popup-content');

    Array.from(popupContents).forEach(function(content) {
      Scrollbar.init(content, {
        damping: 0.05,
      });
    })
  }

  $('.js-open-remind').on('click', function() {
    $(this).fadeOut();
    setTimeout(function() {
      $('.js-remind-form').fadeIn();
    }, 400);
  });

  $('.js-remind-form').on('submit', function(e) {
    e.preventDefault();
    var value = this.querySelector('input[type="email"]').value;
    var button = this.querySelector('button[type="submit"]');

    $(button).fadeOut();

    // Запрос на отправку email
     $.post('/send.php', {email: value}, function() {
      input.classList.add('success');
     });
  });

  if ($('.js-partners-slider').length) {
    $('.js-partners-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      infinite: false,
      responsive: [
        {
          breakpoint: 1023,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
          },
        },
      ]
    });
  }

  // Timer
  var deadLine = new Date(2019, 11, 23, 0, 0, 0);

  function timer (deadline) {
    var daysLeft = document.querySelector('.js-days');
    var hoursLeft = document.querySelector('.js-hours');
    var minutesLeft = document.querySelector('.js-minutes');

    var timeLeft = deadline - new Date();
    var minutes = Math.floor((timeLeft / 1000 / 60) % 60);
    var hours = Math.floor((timeLeft / (1000 * 60 * 60)) % 24);
    var days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));

    daysLeft.innerText = ('0' + days).slice(-2);
    hoursLeft.innerText = ('0' + hours).slice(-2);
    minutesLeft.innerText = ('0' + minutes).slice(-2);
    setInterval(function() {
      var timeLeft = deadline - new Date();
      var minutes = Math.floor((timeLeft / 1000 / 60) % 60);
      var hours = Math.floor((timeLeft / (1000 * 60 * 60)) % 24);
      var days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));

      daysLeft.innerText = ('0' + days).slice(-2);
      hoursLeft.innerText = ('0' + hours).slice(-2);
      minutesLeft.innerText = ('0' + minutes).slice(-2);
    }, 1000);
  };

  // timer(deadLine);

  function updateCompanyCounter(quantity) {
    let companyQuantity = quantity,
      text = numString(quantity, ['участник', 'участника', 'участников']),
      viewQuantuty = document.querySelector('.js-company-counter'),
      viewText = document.querySelector('.js-counter-text');

    if (companyQuantity < 10) {
      companyQuantity = `000${companyQuantity}`;
    } else if (companyQuantity < 100) {
      companyQuantity = `00${companyQuantity}`;
    } else if (companyQuantity < 1000) {
      companyQuantity = `0${companyQuantity}`;
    }
    viewQuantuty.innerText = companyQuantity;
    viewText.innerText = text;
  };

  function numString(number, strings) {
    let number1 = number % 10;
    if (number > 10 && number < 20) { return strings[2]; }
    if (number1 > 1 && number1 < 5) { return strings[1]; }
    if (number1 == 1) { return strings[0]; }
    return strings[2];
  }

  if (chooseHandl) {
    chooseHandl.addEventListener('mousedown', initDrag, false);
    chooseHandl.addEventListener('touchstart', initDrag, false);
  }

  function initDrag(e) {
    let wall = document.querySelector('.js-wall'),
      choosePixel = document.querySelector('.js-choose-pixel'),
      chooseInfo = choosePixel.querySelector('.js-choose-info');

    // choosePixel.classList.add('active');
    chooseInfo.innerText = '';
    startX = e.clientX || Math.floor(e.changedTouches[0].clientX);
    startY = e.clientY || Math.floor(e.changedTouches[0].clientY);
    startWidth = parseInt(window.getComputedStyle(choosePixel).width, 10);
    startHeight = parseInt(window.getComputedStyle(choosePixel).height, 10);
    wall.addEventListener('mousemove', doDrag, false);
    wall.addEventListener('touchmove', doDrag, false);
    wall.addEventListener('mouseup', stopDrag, false);
    chooseHandl.addEventListener('touchend', stopDrag, false);
    wall.addEventListener('touchcancel', stopDrag, false);
    wall.addEventListener('mouseleave', stopDrag, false);
  }

  function doDrag(e) {
    e.preventDefault();
    const MIN_SIZE = 50;
    let wall = document.querySelector('.js-wall'),
      choosePixel = document.querySelector('.js-choose-pixel'),
      wallStyles = getComputedStyle(wall),
      paddingRight = parseInt(wallStyles.paddingRight, 10),
      maxWidth = wall.offsetWidth - choosePixel.offsetLeft - paddingRight,
      maxHeight = wall.offsetHeight - choosePixel.offsetTop,
      clientX = e.clientX || Math.floor(e.changedTouches[0].clientX),
      clientY = e.clientY || Math.floor(e.changedTouches[0].clientY),
      width = startWidth + clientX - startX,
      height = startHeight + clientY - startY;

    if (width < MIN_SIZE) {
      choosePixel.style.width = MIN_SIZE + 'px';
    } else if (width > maxWidth) {
      choosePixel.style.width = maxWidth + 'px';
    } else {
      choosePixel.style.width = width + 'px';
    }

    if (height < MIN_SIZE) {
      choosePixel.style.height = MIN_SIZE + 'px';
    } else if (height > maxHeight) {
      choosePixel.style.height = maxHeight + 'px';
    } else {
      choosePixel.style.height = height + 'px';
    }
  }

  function stopDrag(e) {
    let wall = document.querySelector('.js-wall');

    correctSize();
    wall.removeEventListener('mousemove', doDrag, false);
    wall.removeEventListener('touchmove', doDrag, false);
    wall.removeEventListener('mouseup', stopDrag, false);
    chooseHandl.removeEventListener('touchend', stopDrag, false);
    wall.removeEventListener('mouseleave', stopDrag, false);
  }

  function correctSize() {
    const MAX_SIZE = 50;
    const MIN_SIZE = 25;
    let choosePixel = document.querySelector('.js-choose-pixel'),
      width = parseInt(window.getComputedStyle(choosePixel).width, 10),
      height = parseInt(window.getComputedStyle(choosePixel).height, 10);

    if (width < MIN_SIZE) {
      width = MAX_SIZE + 'px';
    } else if (width % MAX_SIZE >= MIN_SIZE) {
      width = width - (width % MAX_SIZE) + MAX_SIZE + 'px';
    } else if (width % MAX_SIZE < MIN_SIZE) {
      width = width - (width % MAX_SIZE) + 'px';
    }

    if (height < MIN_SIZE) {
      height = MAX_SIZE + 'px';
    } else if (height % MAX_SIZE >= MIN_SIZE) {
      height = height - (height % MAX_SIZE) + MAX_SIZE + 'px';
    } else if (height % MAX_SIZE < MIN_SIZE) {
      height = height - (height % MAX_SIZE) + 'px';
    }

    $(choosePixel).animate({
      width: width,
      height: height,
    }, 300);

    setTimeout(function() {checkIntersect()}, 300);
  }

  function checkIntersect() {
    let choosePixel = document.querySelector('.js-choose-pixel'),
      applyButton = choosePixel.querySelector('.js-apply-choose'),
      chooseInfo = choosePixel.querySelector('.js-choose-info'),
      coords = {
        top: choosePixel.offsetTop,
        left: choosePixel.offsetLeft,
        right: choosePixel.offsetLeft + choosePixel.offsetWidth,
        bottom: choosePixel.offsetTop + choosePixel.offsetHeight,
      };

    for (var i = 0; i < app.busyPixels.length; i++) {
      var pixel = app.busyPixels[i];
      if (
        ((coords.right >= pixel.right || coords.right <= pixel.right) && coords.right > pixel.left) &&
        ((coords.left <= pixel.left || coords.left >= pixel.left) && coords.left < pixel.right) &&
        ((coords.top <= pixel.top || coords.top >= pixel.top) && coords.top < pixel.bottom) &&
        ((coords.bottom >= pixel.bottom || coords.bottom <= pixel.bottom) && coords.bottom > pixel.top)
        ) {
        choosePixel.classList.add('error');
        applyButton.disabled = true;
        break;
      } else {
        choosePixel.classList.remove('error');
        applyButton.disabled = false;
      }
    }

    if (choosePixel.offsetWidth >= 100 && choosePixel.offsetHeight >= 50) {
      if (choosePixel.classList.contains('error')) {
        chooseInfo.innerText = 'Пересечение областей!';
      } else {
        let price = Math.round((choosePixel.offsetWidth / PIXEL_WIDTH) * (choosePixel.offsetHeight / PIXEL_HEIGHT) * app.cost);
        chooseInfo.innerHTML = `Ваш вклад: <span>${price}р</span>`;
      }
    }
  };

  openPopup.addEventListener('click', function(e) {
    e.preventDefault();
    let popupInfo = document.querySelector('.js-info-popup'),
      closePopup = popupInfo.querySelector('.js-close-popup');

    if (!popupInfo.classList.contains('active')) {
      $(popupInfo).addClass('active').fadeIn();
      closePopup.addEventListener('click', function(e) {
        e.preventDefault();
        $(popupInfo).removeClass('active').fadeOut();
      });
    }
  });

  // Проверка устройства
  function checkDevice() {
    if (device.mobile() || device.tablet()) {
      let wallContent = document.querySelector('.js-wall-content'),
        viewWidth = document.documentElement.clientWidth,
        wall = wallContent.querySelector('.js-wall');

        $('.js-scroll-thumb').animate({
          width: `${viewWidth * 100 / wall.offsetWidth}%`,
        }, 100);

      wallContent.addEventListener('scroll', function() {
        let scrollThumbWidth = (viewWidth + this.scrollLeft) * 100 / wall.offsetWidth;

          $('.js-scroll-thumb').css({
            width: `${scrollThumbWidth}%`,
          });
      });

      window.addEventListener('resize', function() {
        viewWidth = document.documentElement.clientWidth;
        let scrollThumbWidth = (viewWidth + wallContent.scrollLeft) * 100 / wall.offsetWidth;

          $('.js-scroll-thumb').css({
            width: `${scrollThumbWidth}%`,
          });
      })
    } else if (device.desktop()) {
      scrollWall();
    }
  }

  // Показываем попап при оплате картой
  function showSuccessPayPopup() {
    let wallContainer = document.querySelector('.js-wall-container'),
      popup = document.querySelector('template').content.querySelector('.js-popup-pay-card').cloneNode(true),
      closePopup = popup.querySelector('.js-close-popup');
    app.closeStepPopup(app.step);
    closePopup.addEventListener('click', function(e) {
      e.preventDefault();
      setTimeout(function() {popup.remove()}, 500);
    });
    wallContainer.append(popup);
    $(popup).fadeIn();
  };

  // Показываем попап Актуальное
  if (relevantOpen) {
    let relevantTemplate = document.querySelector('template').content.querySelector('.js-relevant-item');

    relevantOpen.addEventListener('click', function(e) {
      e.preventDefault();
      if (!this.classList.contains('active')) {
        this.classList.add('active');

        axios.get('data/relevant.json')
          .then(function(response) {
            let popup = document.querySelector('template').content.querySelector('.js-popup-relevant').cloneNode(true),
              closeButton = popup.querySelector('.js-close-popup'),
              relevantContent = popup.querySelector('.js-relevant-content'),
              wallContainer = document.querySelector('.js-wall-container'),
              relevantFragment = document.createDocumentFragment(),
              relevants = response.data.relevant.sort(function(item1, item2) {
              return new Date(item2.date) - new Date(item1.date);
            });

            relevants.forEach(function(relevant) {
              relevantFragment.appendChild(createRelevantItem(relevant));
            });

            relevantContent.prepend(relevantFragment);

            Scrollbar.init(relevantContent, {
              damping: 0.05,
            });

            closeButton.addEventListener('click', closeRelevantPopup);
            wallContainer.append(popup);
            $(popup).fadeIn();
          })
          .catch(function() {
            relevantOpen.classList.remove('active');
          });
      }
    });

    function closeRelevantPopup() {
      let popup = document.querySelector('.js-wall-container .js-popup-relevant'),
        closeButton = popup.querySelector('.js-close-popup');

      closeButton.removeEventListener('click', closeRelevantPopup);
      $(popup).fadeOut();
      setTimeout(function() {
        popup.remove();
        relevantOpen.classList.remove('active');
      }, 500);
    }

    function createRelevantItem({title, text, date, image}) {
      let relevantItem = relevantTemplate.cloneNode(true),
        datePost = new Date(date);

      relevantItem.querySelector('.relevant__title').innerText = title;
      relevantItem.querySelector('.relevant__date').innerText = `${datePost.getDate()} ${Month[datePost.getMonth()]}, ${datePost.getFullYear()}`;

      relevantItem.querySelector('.relevant__date').setAttribute('datetime', `${datePost.getFullYear()}-${('0' + (datePost.getMonth() + 1)).slice(-2)}-${datePost.getDate()}`);
      relevantItem.querySelector('.relevant__text').innerText = text;
      relevantItem.querySelector('.relevant__image img').src = image;

      return relevantItem;
    }
  }

  // Автоскролл

  function scrollWall() {
    let wallContent = document.querySelector('.js-wall-content');
    $(wallContent).mCustomScrollbar({
      axis: 'x',
      documentTouchScroll: false,
      contentTouchScroll: 100,
      callbacks: {
        onInit: function() {
          let viewWidth = document.documentElement.clientWidth,
            scrollThumbWidth = viewWidth * 100 / this.mcs.content.width();

          $('.js-scroll-thumb').animate({
            width: `${scrollThumbWidth}%`,
          }, 100);

        },
        onUpdate: function() {
          let viewWidth = document.documentElement.clientWidth,
            scrollThumbWidth = (viewWidth + this.mcs.left * -1) * 100 / this.mcs.content.width();

          $('.js-scroll-thumb').css({
            width: `${scrollThumbWidth}%`,
          });
        },
        whileScrolling: function() {
          let viewWidth = document.documentElement.clientWidth,
            scrollThumbWidth = (viewWidth + this.mcs.left * -1) * 100 / this.mcs.content.width();

          $('.js-scroll-thumb').css({
            width: `${scrollThumbWidth}%`,
          });
        }
      }
    });

    // wallContent.addEventListener('mouseenter', function() {
    //   $(this).mCustomScrollbar('stop');
    // });

    // wallContent.addEventListener('mouseleave', function() {
    //   let scrollContainer = document.querySelector('.mCSB_container'),
    //     viewWidth = document.documentElement.clientWidth,
    //     wall = document.querySelector('.js-wall');

    //   if (app.step === 0) {
    //     $(this).mCustomScrollbar('scrollTo', 'right', {
    //       scrollEasing: 'linear',
    //       scrollInertia: Math.round((wall.offsetWidth + scrollContainer.offsetLeft - viewWidth) / 20 * 1000)
    //     });
    //   }
    // });
  }
  function autoScrollWall() {
    let wall = document.querySelector('.js-wall'),
    wallContent = document.querySelector('.js-wall-content');

    $(wallContent).mCustomScrollbar('scrollTo', 'right', {
      scrollEasing: 'linear',
      scrollInertia: Math.round(wall.offsetWidth / 20 * 1000)
    });
  }
});
