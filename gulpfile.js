'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const del = require('del');
const autoprefixer = require('autoprefixer');
$.sass.compiler = require('node-sass');

const config = {
  src: {
    root: 'src',
    templates: 'src',
    styles: 'src/style',
    scripts: 'src/script',
    assets: 'src/assets',
  },
  dest: {
    root: 'build',
    templates: 'build',
    styles: 'build/css',
		scripts: 'build/js',
		images: 'build/img',
  },
  jsConcat: [
		'./src/assets/libs/jquery-3.2.1.min.js',
    './src/assets/libs/axios/axios.min.js',
    './src/assets/libs/smooth-scrollbar/smooth-scrollbar.js',
		'./src/assets/libs/slick/slick.min.js',
		'./src/assets/libs/device.js',
		'./src/assets/libs/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js',
		'./src/assets/libs/lazyload/lazyload.min.js',
  ],
  browserSync: {
		reloadOnRestart: true,
		notify: false,
		port: 9000,
		startPath: "/",
		server: {
			baseDir: ['build', 'src', 'src/assets']
		}
	}
}

function errorHandler () {
	var args = Array.prototype.slice.call(arguments);
	const err = args[0]
	$.notify.onError({
		title: 'Error in: '+ err.plugin,
		message: '<%= error.message %>',
		sound: 'Submarine',
	}).apply(this, args);
	this.emit('end');
}

function styles () {
	var plugins = [
		autoprefixer({ overrideBrowserslist: ['last 2 version'], grid: "autoplace"}),
	];
	return gulp.src(config.src.styles + '/*.{sass,scss}')
		.pipe($.plumber({errorHandler: errorHandler }))
		.pipe(
			$.sass({
				includePaths: [config.src.styles, 'node_modules']
			})
		)
		.pipe(
			$.sass({
				// outputStyle: 'compressed', //nested, expanded, compact, compressed
				precision: 5,
				sourcemap: true,
				errLogToConsole: false
			})
		)
		.pipe($.postcss(plugins))
		.pipe(gulp.dest(config.dest.styles))
		.pipe(reload({ stream: true }));
}

function scripts () {
	return gulp.src([config.src.scripts+'/**/*.js'])
		.pipe($.filter(function(file) {
			return !/\_/.test(file.path) && !/^_/.test(file.relative);
		}))
		.pipe($.plumber({errorHandler: errorHandler }))
		.pipe($.rollup({
			input: './src/script/index.js',
			format: 'es'
		}))
		.pipe($.babel({
			presets: ['@babel/preset-env']
		}))
		.pipe(gulp.dest(config.dest.scripts));
}

// concat scripts
function concatScripts () {
	return gulp.src(config.jsConcat)
		.pipe($.concat('vendors.js'))
		.pipe(gulp.dest(config.dest.scripts))
}

function copyHtml () {
  return gulp.src(config.src.templates + '/**/*.html')
    .pipe($.htmlPrettify({ indent_char: '	', indent_size: 1 }))
    .pipe(gulp.dest(config.dest.templates));
}

function clean () {
	return del([config.dest.root])
}

function copyAssets () {
	return gulp.src(config.src.assets + '/**/*.*')
		.pipe(gulp.dest(config.dest.root));
}

const copy = gulp.series(
  copyHtml,
	copyAssets
);

function watch (cb) {
	browserSync.init(config.browserSync);

	// watch for changes
	gulp.watch([
		config.dest.scripts+'/**/*.js',
		'src/img/**/*'
	]).on('change', reload);

	gulp.watch([config.src.scripts+'/**/*.js'], gulp.series('scripts'));
	gulp.watch('src/vendors/**/*.js', gulp.series('concatScripts'));
	gulp.watch(config.src.styles+'/**/*.scss', gulp.series('styles'));

	cb()
}

const serve = gulp.series(
	styles,
	scripts,
	concatScripts,
	watch
);

const build = gulp.series(
	clean,
	styles,
	scripts,
	concatScripts,
	copy
);

exports.scripts = scripts
exports.concatScripts = concatScripts
exports.styles = styles
exports.copy = copy
exports.serve = serve
exports.build = build
exports.clean = clean
exports.watch = watch
exports.default = serve
